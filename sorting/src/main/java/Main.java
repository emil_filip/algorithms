import org.apache.commons.lang3.ArrayUtils;
import sorting.Sort;

import java.util.Random;

public class Main {

    private static Random r = new Random();

    public static void main(String[] args) {
        int[] array = new int[]{};

        System.out.println("Array before sorting: ");
        for (int i = 0; i < 100; i++) {
            final int val = r.nextInt(1_000);
            array = ArrayUtils.add(array, val);
            System.out.println(val);
        }

        long start = System.nanoTime();
        int[] sortedArray = Sort.selectionSort(array);
        long elapsed = System.nanoTime() - start;

        System.out.println("");
        System.out.println("Array after sorting: ");
        for (int element : sortedArray) {
            System.out.println(element);
        }
        System.out.println("");
        System.out.printf("Completed in %,d nanoseconds, i.e. %,f milliseconds.%n", elapsed, (double) elapsed / 1_000_000);
    }
}