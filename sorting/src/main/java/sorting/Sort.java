package sorting;

import org.apache.commons.lang3.ArrayUtils;

public class Sort {

    private Sort() {}

    public static int[] bubbleSort(int[] array) {
        int[] sorted = ArrayUtils.clone(array);
        for (int i = 1; i < sorted.length; i++) {
            int swaps = 0;
            for (int j = 0; j < sorted.length - i; j++) {
                if (sorted[j] > sorted[j + 1]) {
                    ArrayUtils.swap(sorted, j, j + 1);
                    swaps++;
                }
            }
            if (swaps == 0) {
                return sorted;
            }
        }
        return sorted;
    }

    public static int[] selectionSort(int[] array) {
        int[] sorted = ArrayUtils.clone(array);
        for (int i = 0; i < sorted.length; i++) {
            int smallest = i;
            for (int j = i + 1; j < sorted.length; j++) {
                if (sorted[j] < sorted[smallest]) {
                    smallest = j;
                }
            }
            ArrayUtils.swap(sorted, i, smallest);
        }
        return sorted;
    }

    public static int[] quickSort(int[] array) {
        if (ArrayUtils.isEmpty(array) || array.length == 1) {
            return array;
        }

        int pivot = array[Math.round((array.length + 1) / 2) - 1];

        int[] below = new int[]{};
        int[] equal = new int[]{};
        int[] above = new int[]{};

        for (int num : array) {
            if (num < pivot) {
                below = ArrayUtils.add(below, num);
            } else if (num > pivot) {
                above = ArrayUtils.add(above, num);
            } else {
                equal = ArrayUtils.add(equal, num);
            }
        }

        return ArrayUtils.addAll(quickSort(below), ArrayUtils.addAll(equal, quickSort(above)));
    }
}
