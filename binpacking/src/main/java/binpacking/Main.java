package binpacking;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.tuple.MutablePair;

import java.util.Random;

public class Main {

    private static final int numBoxes = 5;
    private static final int maxBinHeight = 10;

    private static final Random r = new Random();

    public static void main(String[] args) {
        int[] boxes = new int[]{};

        System.out.printf("%nThere are %d boxes.%n", numBoxes);
        System.out.printf("The box heights are: %n");
        int totalHeight = 0;
        for (int i = 0; i < numBoxes; i++) {
            int box = r.nextInt(maxBinHeight - 1) + 1;
            boxes = ArrayUtils.add(boxes, box);
            totalHeight += box;
            System.out.printf("%d ", box);
        }
        System.out.printf("%n%nThe maximum bin height is %d.%n", maxBinHeight);
        int optimumBins = (int) Math.ceil((double) totalHeight / maxBinHeight);
        System.out.printf("The lower bound of bins is %d.%n", optimumBins);

        System.out.printf("%nPacking...%n");

        MutablePair<Integer, int[]>[] packed = BinPacking.firstFitDecreasing(boxes, maxBinHeight);
        System.out.printf("%n%d bins used.%n", packed.length);
        System.out.printf("This is %s.%n", packed.length <= optimumBins ? "OPTIMAL" : "NOT OPTIMAL");
        System.out.printf("These are the stacks in each bin, including their (height), <wasted space> and contents:");

        int totalWasted = 0;
        for (MutablePair<Integer, int[]> stack : packed) {
            System.out.printf("%n(%d) \t", stack.getLeft());
            int wasted = maxBinHeight - stack.getLeft();
            totalWasted += wasted;
            System.out.printf("<%d> \t", wasted);
            for (int box : stack.getRight()) {
                System.out.printf("%d ", box);
            }
        }

        System.out.printf("%n%nThe total amount of wasted space is %d.", totalWasted);
    }
}
