package binpacking;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Arrays;
import java.util.Collections;

public class BinPacking {

    private BinPacking() {}

    public static MutablePair<Integer, int[]>[] firstFit(int[] boxes, int maxHeight) {
        MutablePair<Integer, int[]>[] packed = new MutablePair[]{};
        outerloop :
        for (int box : boxes) {
            if (ArrayUtils.isEmpty(packed)) {
                packed = ArrayUtils.add(packed, MutablePair.of(box, new int[]{box}));
            } else {
                for (MutablePair<Integer, int[]> slot : packed) {
                    if (slot.getLeft() + box <= maxHeight) {
                        slot.setLeft(slot.getLeft() + box);
                        slot.setRight(ArrayUtils.add(slot.getRight(), box));
                        continue outerloop;
                    }
                }
                packed = ArrayUtils.add(packed, MutablePair.of(box, new int[]{box}));
            }
        }
        return packed;
    }

    public static MutablePair<Integer, int[]>[] firstFitDecreasing(int[] boxes, int maxHeight) {
        Integer[] arr = ArrayUtils.toObject(ArrayUtils.clone(boxes));
        Arrays.sort(arr, Collections.reverseOrder());
        return firstFit(ArrayUtils.toPrimitive(arr), maxHeight);
    }
}
